/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

#include <stdint.h>
#include <avr/io.h>

#include <util/twi.h>
#include <avr/eeprom.h>

#include "iiadc.h"

/*-----------------------------------------------*/

#define _BST    0x80
#define _SC     0x10
#define _DR1    0x08
#define _DR0    0x04
#define _PGA1   0x02
#define _PGA0   0x01

/* 128 32 16 8 sps */
#define ADS_12  0x00
#define ADS_14  0x01
#define ADS_15  0x02
#define ADS_16  0x03
#define ADS_POS 0x02

/*-----------------------------------------------*/

extern uint8_t channel;
extern uint8_t adcreg;
extern uint8_t adcgain;
extern uint8_t adcbits;
extern int16_t adcdata;
extern int16_t adcresult;
extern int32_t adcresult32;

/*-----------------------------------------------*/

struct dch {
	int16_t uoffset[2][3];
	uint32_t ugain[2][3];
	uint8_t ocr2[2];
	uint8_t crc;
} datachannel;

struct dch eedatachannel EEMEM;

/*-----------------------------------------------*/

void delay_x1ms (uint8_t delay);

/*-----------------------------------------------*/

void iic_init ( void ) {

	/*345600
	TWBR = 2;
	TWBR = 10;*/
	
	TWBR = 16;
	TWCR = 1<<TWEN;
	
	/*
	TWBR bitrate;
	TWCR control;
	TWSR status
	TWDR data
	TWAR adr
	*/

	adcbits = 16;
	adcgain = 2;
}

/*-----------------------------------------------*/

uint8_t readadcconv ( int16_t * data ) {

	uint16_t conv;
	uint8_t status;
	uint8_t retv;
	
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while(!(TWCR&(1<<TWINT)));
	if( (TWSR&0xF8) != TW_START ) return 10;
	
	retv = 0;
	
	while(1){
		TWDR = 0x90 | TW_READ;
		TWCR = (1<<TWINT)|(1<<TWEN);
		while(!(TWCR&(1<<TWINT)));
		if( (TWSR&0xF8) != TW_MR_SLA_ACK ) {
			retv = 11;
			break;
		}
		
		TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
		while(!(TWCR&(1<<TWINT)));
		if( (TWSR&0xF8) != TW_MR_DATA_ACK ) {
			retv = 12;
			break;
		}
		conv = TWDR;
		
		TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
		while(!(TWCR&(1<<TWINT)));
		if( (TWSR&0xF8) != TW_MR_DATA_ACK ) {
			retv = 12;
			break;
		}
		
		conv <<= 8;
		conv += TWDR;
		
		TWCR = (1<<TWINT)|(1<<TWEN);
		while(!(TWCR&(1<<TWINT)));
		if( (TWSR&0xF8) != TW_MR_DATA_NACK ) {
			retv = 13;
			break;
		}
		
		status = TWDR;
		break;
	}

	TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
	if(retv) return retv;

	if(status&_BST) return -1;
	
	*data = conv;
	
	return 0;
}

/*-----------------------------------------------*/

uint8_t setadc ( void ) {

	uint8_t retv;
	
	adcreg = 0;
	if(adcgain==8) adcreg |= 3;
	else if(adcgain==4) adcreg |= 2;
	else if(adcgain==2) adcreg |= 1;
	else if(adcgain==1) adcreg |= 0;
	else adcgain = 1;
	
	if(adcbits==12) adcreg |= (ADS_12<<ADS_POS);
	else if(adcbits==14) adcreg |= (ADS_14<<ADS_POS);
	else if(adcbits==15) adcreg |= (ADS_15<<ADS_POS);
	else if(adcbits==16) adcreg |= (ADS_16<<ADS_POS);
	else adcbits = 12;
	
	adcreg |= (_BST) | (_SC);
	
	/*start*/
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	/*wait*/
	while(!(TWCR&(1<<TWINT)));
	/*check*/
	if( (TWSR&0xF8) != TW_START ) return 1;
	retv = 0;
	
	while(1){
		TWDR = 0x90 | TW_WRITE;
		TWCR = (1<<TWINT)|(1<<TWEN);
		while(!(TWCR&(1<<TWINT)));
		if( (TWSR&0xF8) != TW_MT_SLA_ACK ) {
			retv = 2;
			break;
		}
		
		TWDR = adcreg;
		TWCR = (1<<TWINT)|(1<<TWEN);
		while(!(TWCR&(1<<TWINT)));
		if( (TWSR&0xF8) != TW_MT_DATA_ACK ) {
			retv = 3;
			break;
		}
		break;
	}
	
	/* stop */
	TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
	return retv;
}

/*-----------------------------------------------*/

uint8_t getadcdata (void) {
	
	uint8_t ret, del;

	ret = setadc();

	/* delay
	16bit 200ms  5sps
	15bit 100ms 10sps
	14bit 50ms  20sps
	12bit 20ms  50sps
	*/
	del = 180;
	if(adcbits==12) del = 18;
	else if(adcbits==14) del = 46;
	else if(adcbits==15) del = 90;
	
	delay_x1ms (del);
	if(ret) return ret;
	
	if ( 0 != ( ret = readadcconv(&adcdata) ) ) return ret;
	
	/* because reverse amplify */
	adcresult = -adcdata;
	
	if(adcbits==12) adcresult *= 16;
	else if(adcbits==14) adcresult *= 4;
	else if(adcbits==15) adcresult *= 2;

	if(adcgain==1) adcresult += 8192;
	if(adcgain==2) adcresult += 2*8192;
	
	return 0;
}

/*-----------------------------------------------*/
/* calculation adcresult to calibration value
0 - don't calculation
1,2,3 2v5 25V 100V
4-8 user define
*/

void adccal ( void ) {
	
	int64_t r64;
	
	adcresult32 = adcresult;
	
	if((adcgain<3)&&(channel>0)&&(channel<4)){
		
		adcresult32 -= datachannel.uoffset[adcgain-1][channel-1];
		r64 = adcresult32;
		r64 *= datachannel.ugain[adcgain-1][channel-1];
		r64 >>= 16;
		adcresult32 = r64;
	}
}

/*-----------------------------------------------*/

void setoff ( int16_t offset) {
	
	if((adcgain<3)&&(channel>0)&&(channel<4))
		datachannel.uoffset[adcgain-1][channel-1] = offset;
}

/*-----------------------------------------------*/

void setugain ( uint32_t v ) {
	
	uint32_t a,i;
	uint32_t tmp;
	int64_t r64;
	
	if((adcgain<3)&&(channel>0)&&(channel<4)) {
		
		adcresult32 = adcresult;
		adcresult32 -= datachannel.uoffset[adcgain-1][channel-1];
		if(adcresult32<0) adcresult32 = -adcresult32;
		
		for( a=0, i=0x800000; ; ) {
			a |= i;
			r64 = adcresult32;
			r64 *= a;
			r64 >>= 16;
			tmp = r64;
			if( v < tmp ) a &= ~(i);
			if(i==0) break;
			i >>= 1;
		}
		
		datachannel.ugain[adcgain-1][channel-1] = a;
	}
}

/*-----------------------------------------------*/
/* read and save from eeprom */

uint8_t ccrc ( void * dp, uint8_t siz ) {
	
	uint8_t * d;
	uint8_t crc;
	
	d = dp;
	crc = 0xff;
	
	while(siz--) crc -= *d++;
	
	return crc;
}

/*-----------------------------------------------*/

void writdch( void ) {
	
	datachannel.crc = ccrc ( &datachannel, sizeof(datachannel)-1 );
	eeprom_write_block (&datachannel, &eedatachannel, sizeof(datachannel));
}

/*-----------------------------------------------*/

uint8_t readdch( void ) {
	
	eeprom_read_block (&datachannel, &eedatachannel, sizeof(datachannel) );
	
	if( datachannel.crc == ccrc ( &datachannel, sizeof(datachannel) -1 ) )
		return 0;
	
	return 1;
}

/*-----------------------------------------------*/

uint8_t initcal (void) {
	
	if( readdch() ) {
		uint8_t i;
		
		for(i=0;i<2;++i) {
			datachannel.uoffset[i][0] = 0;
			datachannel.uoffset[i][1] = 0;
			datachannel.uoffset[i][2] = 0;
		
			datachannel.ugain[i][0] = 65537;
			datachannel.ugain[i][1] = 65537;
			datachannel.ugain[i][2] = 65537;
			
			datachannel.ocr2[i] = 127;
		}

		writdch();
		return 1;
	}
	
	return 0;
}

/*-----------------------------------------------*/

uint8_t getocr2 (void) {
	
	if( adcgain < 3 )
		return datachannel.ocr2[adcgain-1];

	return datachannel.ocr2[0];
}

/*-----------------------------------------------*/

void setocr2 (uint8_t ocr) {
	if( adcgain < 3 )
		datachannel.ocr2[adcgain-1] = ocr;
}

/*-----------------------------------------------*/
