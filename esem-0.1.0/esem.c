/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------

-----------------------------------------------*/
/*
l: 3f
h: c9
*/
/* cc3f */

#include <stdint.h>
#include <string.h>

/*-----------------------------------------------*/

#if (__AVR_ARCH__ > 1)
#include <compat/ina90.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
/*
#include <avr/sleep.h> 
#include <stdlib.h>
*/
#include <avr/wdt.h>
#endif

/*-----------------------------------------------*/
/* configuracje fuse i configi hardware uC */
/*-----------------------------------------------*/

#include "Bconf.h"

#define TIMERGLOB 0
/* 0,1,2 div one PWM to 1,2,4 */
#define PWMDIV    2

/*-----------------------------------------------*/
/* programowe h */
/*-----------------------------------------------*/

#include "util.h"
#include "iiadc.h"
#include "uarttx.h"

/*-----------------------------------------------*/

#if TIMERGLOB == 1
volatile uint8_t fl_ms = 0;
volatile uint8_t fl_s = 0;
volatile uint8_t fl_m = 0;
volatile uint8_t fl_h = 0;
#endif

/*-----------------------------------------------*/

const char version[] PROGMEM = "trol.six, MIT license, 0.1.0 ESEM\n";

/*-----------------------------------------------*/

char *rsirq;
char *rsinbuf;
char rsbuf1[32];
char rsbuf2[32];
static uint8_t getrs;

uint8_t fun;
	
char tbl[11];

uint16_t datrstr[7];

/*-----------------------------------------------*/

uint8_t readnumb ( void ) {
	uint8_t ile;
	char * wsk;
	uint32_t t32;
	
	wsk = &rsinbuf[3];
	
	for ( ile = 0; ile<sizeof(datrstr)/sizeof(datrstr[0]); ++ile ) {
		wsk = getnumber32( wsk, &t32 );
		if ( !wsk ) break;
		datrstr[ile] = t32;
	}
	
	return ile;
}

/*-----------------------------------------------*/

int16_t adcdata;
int16_t adcresult;
int32_t adcresult32;

#if PWMDIV == 1
int16_t T1CRA[2];
int16_t T1CRB[2];
int16_t T1CRAB;
int16_t T1CRBB;
#elif  PWMDIV == 2
int16_t T1CRA[4];
int16_t T1CRB[4];
int16_t T1CRAB;
int16_t T1CRBB;
#endif

uint8_t adcreg;
uint8_t adcgain;
uint8_t adcbits;
uint8_t channel;
uint16_t delay10ms;

volatile uint8_t delaytmp;

/*-----------------------------------------------*/

void (* wskfun)(void);

/*-----------------------------------------------*/

#if PWMDIV == 1
void setocr( void ) {
	T1CRA[0] = T1CRAB>>1;
	T1CRA[1] = (T1CRAB>>1)+(T1CRAB&0x01);
	T1CRB[0] = T1CRBB>>1;
	T1CRB[1] = (T1CRBB>>1)+(T1CRBB&0x01);
}
#elif PWMDIV == 2
void setocr( void ) {
	int16_t tmp;
	tmp = T1CRAB>>2;
	T1CRA[0] = tmp;
	T1CRA[1] = tmp+(T1CRAB&0x01);
	T1CRA[2] = tmp;
	T1CRA[3] = tmp+(T1CRAB&0x02);
	tmp = T1CRBB>>2;
	T1CRB[0] = tmp;
	T1CRB[1] = tmp+(T1CRBB&0x01);
	T1CRB[2] = tmp;
	T1CRB[3] = tmp+(T1CRBB&0x02);
}
#endif
/*-----------------------------------------------*/

inline void fu00 (void);
inline void fu01 (void);
inline void fu02 (void);
inline void fu03 (void);
inline void fu04 (void);
inline void fu05 (void);
inline void fu06 (void);
inline void fu07 (void);
inline void fu08 (void);

void (* const pff[9])(void) PROGMEM = {
	fu00, fu01, fu02, fu03, fu04, fu05, fu06, fu07, fu08
};

/*-----------------------------------------------*/

void delay_x1ms (uint8_t delay) {
	while(delay--) _delay_ms(1);
}

/*-----------------------------------------------*/

void SendBad(void){
	RSout( 'b' ); RSout( '\n' );
}

void SendOK(void){
	RSout( 'd' ); RSout( '\n' );
}

/*-----------------------------------------------*/

void sendadcres (void) {

	int32_t tr;
	
	tr = adcresult32;
	if(tr<0) { RSout( '-' ); tr = -tr; }
	else RSout( '+' );
	binbcd1 ( (void*)tbl, tr );
	
	if((adcgain<3)&&(channel>0)&&(channel<4)){
		RSouttbl ( 7, (void*)tbl );
	} else {
		RSouttbl ( 5, (void*)tbl );
	}
	RSout( '\n' );
}

/*-----------------------------------------------*/

void sendret ( uint8_t ret ) {
	binbcd1 ( (void*)tbl, ret );
	RSouttbl ( 3, (void*)tbl );
	RSout( '\n' );
}

/*-----------------------------------------------*/
/* A nothing ? sleep? */

inline void fu00 (void) {

}

/*-----------------------------------------------*/
/* B set PWM 1,2 */
inline void fu01 (void) {
	uint32_t t32;
	
	if ( ( rsinbuf[4] != ' ' ) ||
		( !(getnumber32( &rsinbuf[5], &t32 ) ) ) ||
		( t32>0x0FFF ) ) {
		SendBad();
	} else {
		#if PWMDIV
		if ( rsinbuf[3] == '1') T1CRAB = t32;
		if ( rsinbuf[3] == '2') T1CRBB = t32;
		setocr();
		#else
		if ( rsinbuf[3] == '1') OCR1A = t32;
		if ( rsinbuf[3] == '2') OCR1B = t32;
		#endif
		SendOK();
	}
	fun = 0;
}

/*-----------------------------------------------*/
/* C */
inline void fu02 (void) {
	RSout( 'f' );
	RSout( 'u' );
	RSout( '2' );
	RSout( '\n' );
	_delay_ms(400);
}

/*-----------------------------------------------*/
/* D set ADC gain bit */

inline void fu03 (void) {
	uint8_t g,b;
	
	fun = 0;
	
	while(1){
		if ( readnumb () < 2) break;
		g = datrstr[0];
		b = datrstr[1];
		
		if ( (g!=8) && (g!=4) && (g!=2) && (g!=1) )
			break;
		if ( (b!=12) && (b!=14) && (b!=15) && (b!=16) )
			break;
		
		adcbits = b;
		adcgain = g;
		OCR2 = getocr2();
		
		SendOK();
		return;
	}
	
	SendBad();
}

/*-----------------------------------------------*/
/* E zmierz channel */

inline void fu04 (void) {
	uint32_t t32;
	
	if ( NULL == (getnumber32( &rsinbuf[3], &t32 ) ) ) {
		SendBad();
		fun = 0;
		return;
	}
	channel = t32;
	
	if ( (channel>8) || ( getadcdata() ) ) {
		SendBad();
	} else {
		adccal ();
		sendadcres();
	}
	fun = 0;
}

/*-----------------------------------------------*/

void wait (void) {
	
	uint16_t time;
	uint8_t cykl;
	
	time = delay10ms;
	wdt_reset();
	
	/* max 150*128 ds */
	if(time > (150<<7) ) time = 150 << 7;
	
	for ( cykl=1; time>151; time >>= 1 )
		cykl <<= 1;
	
	while(cykl--){
		wdt_reset();
		while(delaytmp);
		if( ! ( UCSRB & (1<<RXEN) ) ) break;
		cli();
		delaytmp = time;
		sei();
	}
}

/*-----------------------------------------------*/
/* F mierz channel with_delay */

inline void fu05 (void) {

	if ( readnumb () < 2) return;
	channel = datrstr[0];
	delay10ms = datrstr[1];

	while(1){
		wait();
		if ( getadcdata() ) {
			break;
		} else {
			adccal ();
			sendadcres();
		}
		if( ! ( UCSRB & (1<<RXEN) ) ) return;
	}
	SendBad();
	fun = 0;
}

/*-----------------------------------------------*/
/* G CH_PWM CH_U PWM_start stop step delay */

inline void fu06 (void) {
	
	uint8_t ch;
	int16_t ta, to, te; /* start stop step */

	fun = 0;
	
	while(1) {
		
		if ( readnumb () < 6) break;
		ch = datrstr[0];
		channel = datrstr[1];
		ta = datrstr[2];
		to = datrstr[3];
		te = datrstr[4];
		delay10ms = datrstr[5];
		
		if(ta>4095) break;
		if(to>4095) break;
		if(te>4095) break;
		/* if(delay10ms>150) break; */
		
		/* from top to bottom */
		if( ta > to ) te = -te;
		
		/* pre start 300ms */
		cli();
		delaytmp = 30;
		sei();
		
		while(1){
		
			#if PWMDIV
			if( ch == 1 ) { T1CRAB = ta; RSout( '1' ); }
			if( ch == 2 ) { T1CRBB = ta; RSout( '2' ); }
			setocr();
			#else
			if( ch == 1 ) { OCR1A = ta; RSout( '1' ); }
			if( ch == 2 ) { OCR1B = ta; RSout( '2' ); }
			#endif
			
			/* send ch PWM data_adc */
			RSout( ' ' );
			binbcd1 ( (void*)tbl, (uint32_t)ta );
			RSouttbl ( 4, (void*)tbl );
			RSout( ' ' );
			
			/* time for set pwm */
			
			wait();
			if( ! ( UCSRB & (1<<RXEN) ) ) {
				RSout( '\n' );
				return;
			}
			if(getadcdata())	{
				RSout( '\n' );
			} else {
				adccal ();
				sendadcres();
			}
			
			ta += te;
			
			if(te&0x8000) {
				if ( ta >= to ) continue;
			} else if ( ta <= to ) continue;

			RSout( 0x04 );
			return;
		}
	
		break;
	}
	
	SendBad();
}
/*-----------------------------------------------*/
/* H */

inline void fu07 (void) {
	fu00 ();
}
/*-----------------------------------------------*/
/* I kalibracja

#I kanal U
0 - set pwm for zero
1 - cal offset
2 - cal gain

*/
	
inline void fu08 (void) {
	
	uint32_t t32;
	
	/* gain and 16 bit */
	
	adcbits = 16;
	delay10ms = 40;

	if ( readnumb () < 2) {
		SendBad();
		return;
	}
	
	RSout( 'I' );
	fun = 0;
	
	for ( adcgain=1; adcgain<3; ++adcgain ) {
		
		wait();
		
		/* set pwm */
		
		if ( datrstr[0] == 0 ) {
			uint8_t i;
			
			OCR2 = 0;
			
			for ( i=8; i; ) {
				--i;
				OCR2 |= 1<<i;
				RSout( '.' );
				wait();
				if ( getadcdata() ) {
					SendBad();
					return;
				}
				if( adcresult < 0 )
					OCR2 &= ~(1<<i);
			}

			setocr2 (OCR2);
			continue;
		} else {
			OCR2 = getocr2();
			channel = datrstr[1];
			
			wait();
			if ( getadcdata() ) {
				SendBad();
				return;
			}
		}
		
		/* for offset */
		if ( datrstr[0] == 1 ) {
			setoff ( adcresult );
			continue;
		}
		
		/* for gain */
		if ( datrstr[0] == 2 ) {
			void * wsk;
			wsk = &rsinbuf[5];
			wsk = getnumber32( wsk, &t32 );
			wsk = getnumber32( wsk, &t32 );
			
			if(wsk==NULL){
				SendBad();
				return;
			}

			setugain ( t32 );
			continue;
		}
	}
	
	writdch();
	RSout( '\n' );
}

/*-----------------------------------------------

-----------------------------------------------*/

void showver ( void ) {
	char * tmp;
	tmp = (void*) version;
	while(1) {
		char a;
		a = pgm_read_byte(tmp++);
		RSout( a );
		if(a<' ') break;
	}
}

/*-----------------------------------------------

-----------------------------------------------*/

int main ( void ) {
	
	uint8_t ret;
	
	rsinbuf = rsbuf1;
	rsirq = rsbuf2;
	
	initport();
	setocr();
	uart_init();
	iic_init();
	setadc ();
	getadcdata();
	
	ret = initcal();
	if(ret) sendret ( ret );
	
	OCR2 = getocr2(); /* 1/2 scale ok 1,2V */

	getrs = 0;
	fun = 0;
	delay10ms = 50;

	showver();

	RSout( '\n' );
	
	sei();
	
	while(1) {
		uint16_t *tmp16;
		
		wdt_reset();

		/* frame analyze */
		while( !( UCSRB & (1<<RXEN) ) ) {
			
			if( rsinbuf == rsbuf1 ) {
				rsirq = rsbuf1;
				rsinbuf = rsbuf2;
			} else {
				rsirq = rsbuf2;
				rsinbuf = rsbuf1;
			}
			
			if(rsinbuf[0] != '#') break;
			UCSRB = (1<<TXEN)|(1<<RXEN)|(1<<RXCIE);
			
			fun = rsinbuf[1];
			delay_x1ms (100);
			
			/* version */
			if(fun=='V') {
				fun = 0;
				showver();
			}
			fun -= 'A';
			break;
		}


		tmp16 = (void*)pff;
		if ( fun < (sizeof(pff)/sizeof(*pff)) ) tmp16 += fun;
		wskfun = (void(*)(void)) (uint16_t) pgm_read_word(tmp16);
		wskfun();
		
	}

	return 0;
}

/*-----------------------------------------------*/
/* irq avra */
/*-----------------------------------------------*/

ISR(USART_RXC_vect) {
	static uint8_t prs;
	uint8_t tmp;
	
	tmp = UDR;
	if(tmp == '#') prs = 0;
	else if (prs==0) return;
		
	if ( prs < sizeof(rsbuf1) ) {
		rsirq[prs] = tmp;

		if (tmp=='\n') {
			rsirq[prs] = 0;
			UCSRB = (1<<TXEN); /* off RX rs */
			prs = 0;
		} else ++prs;
		
	} else prs = 0;
}


/*-----------------------------------------------*/

ISR(TIMER0_OVF_vect) {
	
	if(delaytmp)--delaytmp;
	TCNT0 = TCNT0-108;

#if TIMERGLOB == 1
	while(1){
		if(++fl_ms<100) break;
		fl_ms = 0;
		if(++fl_s<60) break;
		fl_s = 0;
		if(++fl_m<60) break;
		fl_m = 0;
		if(++fl_h<24) break;
		fl_h = 0;
		break;
	}
#endif
}

/*-----------------------------------------------*/

ISR(TIMER1_OVF_vect) {
#if PWMDIV == 1
	static uint8_t wskb;
	if(++wskb>1)wskb=0;
	OCR1A = T1CRA[wskb];
	OCR1B = T1CRB[wskb];
#elif PWMDIV == 2
	static uint8_t wskb;
	if(++wskb>3)wskb=0;
	OCR1A = T1CRA[wskb];
	OCR1B = T1CRB[wskb];
#endif
}

/*-----------------------------------------------*/

inline void initport (void) {

	PORTB = 0xFF;
	PORTC = 0xFF;
	PORTD = 0xFF;
	DDRD  = (1<<PD1)|(1<<PD4);
	DDRB  = (1<<PB1)|(1<<PB2)|(1<<PB3); /* PWM1 A B PWM2 */
	
/* watchdog */
	wdt_enable(WDTO_2S);

/*	
fast PWM ICR1 2,7kHz 0x0FFF -> 4096 set diff value write
fast PWM ICR1 5,4kHz 0x07FF -> 2048 set two diff value write
fast PWM ICR1 10,8kHz 0x03FF -> 1024 set four diff value write
WGM 1110
*/
	TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<WGM11);
	TCCR1B = (1<<WGM13)|(1<<WGM12)|(1<<CS10);
#if PWMDIV == 1
	T1CRAB = 2048;
	T1CRBB = 2048;
	ICR1 = 0x07FF;
	OCR1A = 0x03FF;
	OCR1B = 0x03FF;
#elif PWMDIV == 2
	T1CRAB = 2048;
	T1CRBB = 2048;
	ICR1 = 0x03FF;
	OCR1A = 0x01FF;
	OCR1B = 0x01FF;
#else
	ICR1 = 0x0FFF;
	OCR1A = 0x07FF;
	OCR1B = 0x07FF;
#endif
/* fast PWM 256 43,2kHz */
	TCCR2 = (1<<WGM20)|(1<<WGM21)|(1<<COM21)|(1<<CS20);
	OCR2 = getocr2(); /* 1/2 scale ok 1,2V */

/* T0 1024 perscaler irq 255-108 100hz */
	TCCR0 = (5<<CS00);
	TIMSK = (1<<TOIE0)|(1<<TOIE1);

	return;
}

/*-----------------------------------------------*/

