/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

#include <stdint.h>
#include <avr/io.h>

#include "uarttx.h"

/*-----------------------------------------------

-----------------------------------------------*/

#define BAUD (57600UL * 8)
/*#define BAUD (9600UL * 8)*/
/*#define BAUD (19200UL * 8)*/

inline void uart_init ( void ) {
	/*
	UBRR0 = (uint16_t) (F_CPU / BAUD) - 1;
	UCSR0A = 1<<U2X0;
	UCSR0C = 3<<UCSZ00;
	UCSR0B = 1<<TXEN0;
	UDR0;*/
	
	UBRRL = (F_CPU / BAUD) - 1;
	UBRRH = ( (F_CPU / BAUD) - 1 ) >> 8;
	
	UCSRA = 1<<U2X;
	UCSRC = (3<<UCSZ0)|(1<<URSEL);
	UCSRB = (1<<TXEN)|(1<<RXEN)|(1<<RXCIE);
	UDR;
}

/*-----------------------------------------------*/

void RSout( uint8_t d ) {
	/*
	while ( 0 == ( UCSR0A & (1<<UDRE0) ) );
	UDR0 = d;*/
	while ( 0 == ( UCSRA & (1<<UDRE) ) );
	UDR = d;
}

/*-----------------------------------------------*/

void RSouttbl ( unsigned char siz, const unsigned char *tblf ) {
	if ( !siz ) {
		while(1) {
			RSout ( *tblf );
			++tblf;
			if ( *tblf == 0 ) break;
		}
		return;
	}
	while(siz) RSout ( tblf[--siz] );
}

/*-----------------------------------------------*/

