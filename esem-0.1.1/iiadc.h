
void iic_init ( void );

uint8_t readadcconv ( int16_t * data );
uint8_t setadc ( void );
uint8_t getadcdata (void);

uint8_t getocr2 (void);
void setocr2 (uint8_t ocr);

void adccal (void);
uint8_t initcal (void);
void writdch( void );
void setoff ( int16_t offset );
void setugain ( uint32_t v );
