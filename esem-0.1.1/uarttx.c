/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "uarttx.h"

/*-----------------------------------------------

-----------------------------------------------*/

#define UARTTXBUF 1

/*-----------------------------------------------*/

#if UARTTXBUF==1

/* this must be power of 2 and >= 4 */
#define UART1BUF 16
#define UART1MSK (UART1BUF-1)
#define UART1FIL (UART1BUF-2)

static volatile uint8_t wsrsbufin;
static volatile uint8_t wsrsbufou;
static char rsbuf[UART1BUF];

#endif

/*-----------------------------------------------

-----------------------------------------------*/

#define BAUD (57600UL * 8)
/*#define BAUD (9600UL * 8)*/
/*#define BAUD (19200UL * 8)*/

inline void uart_init ( void ) {
	/*
	UBRR0 = (uint16_t) (F_CPU / BAUD) - 1;
	UCSR0A = 1<<U2X0;
	UCSR0C = 3<<UCSZ00;
	UCSR0B = 1<<TXEN0;
	UDR0;*/
	
	UBRRL = (F_CPU / BAUD) - 1;
	UBRRH = ( (F_CPU / BAUD) - 1 ) >> 8;
	
	UCSRA = 1<<U2X;
	UCSRC = (3<<UCSZ0)|(1<<URSEL);
	UCSRB = (1<<TXEN)|(1<<RXEN)|(1<<RXCIE);
	UDR;
}

/*-----------------------------------------------*/

void RSout( uint8_t d ) {

	#if UARTTXBUF==1
	while(1){
		uint8_t howfill;
		howfill = (wsrsbufin-wsrsbufou) & UART1MSK;
		if ( howfill < UART1FIL ) break;
	}
	rsbuf[wsrsbufin] = d;
	wsrsbufin = (1+wsrsbufin) & UART1MSK;
	cli();
	UCSRB |= (1<<UDRIE);
	sei();
	#else
	/*
	while ( 0 == ( UCSR0A & (1<<UDRE0) ) );
	UDR0 = d;*/
	while ( 0 == ( UCSRA & (1<<UDRE) ) );
	UDR = d;
	#endif
}

/*-----------------------------------------------*/

void RSouttbl ( unsigned char siz, const unsigned char *tblf ) {
	while(siz) { RSout ( tblf[--siz] ); if(siz==0) return; }
	while(*tblf) { RSout (*tblf); ++tblf; }
}

/*-----------------------------------------------*/

#if UARTTXBUF==1

ISR(USART_UDRE_vect) {
	/* bufor empty disble irq */
	if ( wsrsbufin == wsrsbufou ) {
		UCSRB &= ~(1<<UDRIE);
	} else {
		UDR = rsbuf[wsrsbufou];
		wsrsbufou = (1+wsrsbufou) & UART1MSK;
	}
}

#endif

/*-----------------------------------------------*/


